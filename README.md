# Linter

This image is only used to install the depencies for linting the different GNU Health repositories.

That way we can reduce our CI pipeline durations.

We only install PyPI packages for linting Python, Sphinx & Ansible and checking REUSE compliance.

Besides the Ansible linting needs some Ansible Collections in place.
