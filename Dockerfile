# SPDX-FileCopyrightText: 2024 Gerald Wiese <wiese@gnuhealth.org>
# SPDX-FileCopyrightText: 2024 Leibniz University Hannover
#
# SPDX-License-Identifier: GPL-3.0-or-later

FROM python:3.12-bookworm

RUN pip3 install flake8 sphinx-lint reuse ansible-lint bandit pip-audit && \
    ansible-galaxy collection install community.crypto community.general community.postgresql ansible.posix
